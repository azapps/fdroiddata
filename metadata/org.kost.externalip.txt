Categories:Internet
License:GPLv3
Web Site:https://github.com/kost/external-ip/blob/HEAD/README.md
Source Code:https://github.com/kost/external-ip
Issue Tracker:https://github.com/kost/external-ip/issues
FlattrID:4449128
Bitcoin:1KbtLnxp6mhkGznFNZZQdcaCUQHmrTtLm4
Litecoin:LfWH3KchBMKdXSfbUXy4iDB3Drgcq3TjH3

Auto Name:External IP
Summary:Display your IP addresses
Description:
Simple application to display IP addresses of interface and your internet IP
address.
.


Repo Type:git
Repo:https://github.com/kost/external-ip.git

Build:2.3,7
    commit=ddc4018d9e
    target=android-8

# Backup of old repo on code.google.com
#Repo Type:hg
#Repo:https://code.google.com/p/external-ip
#
#Build:1.2,3
#    commit=431da48bfb10
#    target=android-8
#
#Build:1.2-change_server,4
#    commit=431da48bfb10
#    patch=change_server.patch
#    forceversion=yes
#    forcevercode=yes
#    target=android-8
#
# Build:1.2-change_server-2,5
#    commit=431da48bfb10
#    patch=change_server-2.patch
#    forceversion=yes
#    forcevercode=yes
#    prebuild=sed -i 's/android:minSdkVersion=\"3\"/android:minSdkVersion=\"4\"/g' AndroidManifest.xml
#    target=android-8
#
#Build:2.2,6
#    commit=13ebec22f40c
#    target=android-8

Auto Update Mode:None
#No revisions in a long time
Update Check Mode:Static
Current Version:2.3
Current Version Code:7


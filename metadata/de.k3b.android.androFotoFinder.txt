Categories:Multimedia
License:GPLv3
Web Site:https://github.com/k3b/androFotoFinder/wiki
Source Code:https://github.com/k3b/androFotoFinder
Issue Tracker:https://github.com/k3b/androFotoFinder/issues
Changelog:https://github.com/k3b/androFotoFinder/wiki/History
Donate:http://donate.openstreetmap.org/

Auto Name:a Foto Finder
Summary:Find local photos by folder, date and/or location
Description:
Search through you local media store for photos.

Features:

* Can handle big image collections (15000+ images in 1000+ folders).
* Uses Android's image content-provider. No initial image scan neccessary.
* Find images via folder (with hirachical picker), wildcards that match folder/filename, date and/or area (if gps-exif data exists).
* Show results in a scrollable gallery view.
* Detail view features zooming, swiping for next/previous image and extended image info (Exif, IPTC, XMP, ICC).
* The geografic map shows markers at places where fotos were taken.

Required Android Permissions:

* INTERNET: to download map data from Open Streetmap Server
* ACCESS_NETWORK_STATE and ACCESS_WIFI_STATE: to find out if wifi/internet is online to start downloaded geodata
* WRITE_EXTERNAL_STORAGE (to cache downloaded map data in local file system and to load gpx/kml-Files to be displayed in the map)
* ACCESS_FINE_LOCATION and ACCESS_COARSE_LOCATION: to display my own location in the map, too
.

Repo Type:git
Repo:https://github.com/k3b/androFotoFinder.git

Build:0.3.4.150728,4
    commit=v0.3.4.150728
    subdir=app
    gradle=yes

Maintainer Notes:
Please use git-branch "FDroid" for FDroid-builds.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.3.4.150728
Current Version Code:4


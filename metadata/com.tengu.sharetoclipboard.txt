Categories:Office
License:GPLv3
Web Site:
Source Code:https://github.com/tengusw/share_to_clipboard
Issue Tracker:https://github.com/tengusw/share_to_clipboard/issues

Auto Name:Share to Clipboard
Summary:Copy to clipboard from the Share menu
Description:
Adds a "Copy to clipboard" function to the default "Share" menu of all
apps and hooks into the Android native Share system for seamless
integration.
.

Repo Type:git
Repo:https://github.com/tengusw/share_to_clipboard

Build:1.0.1,2
    commit=v1.0.1
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/appcompat-v7/acompile "com.googlecode.ez-vcard:ez-vcard:0.9.6"' build.gradle

Build:1.0.2,3
    commit=v1.0.2
    subdir=app
    gradle=yes
    rm=app/libs/*.jar

Build:1.0.3,4
    commit=v1.0.3
    subdir=app
    gradle=yes
    rm=app/libs/*.jar

Build:1.0.5,6
    commit=v1.0.5
    subdir=app
    gradle=yes
    rm=app/libs/*.jar

Build:1.0.6,7
    commit=v1.0.6
    subdir=app
    gradle=yes
    rm=app/libs/*.jar

Build:1.0.7,8
    commit=310e6f9e7ededc1dc1c9f6f49cd44bc07917010e
    subdir=app
    gradle=yes
    rm=app/libs/*.jar

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0.7
Current Version Code:8


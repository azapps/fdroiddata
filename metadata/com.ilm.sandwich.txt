Categories:Navigation
License:Apache2
Web Site:http://smartnavi-app.com/
Source Code:https://github.com/Phantast/smartnavi
Issue Tracker:https://github.com/Phantast/smartnavi/issues

Auto Name:SmartNavi
Summary:Navigation for pedestrians
Description:
Step based and GPS independent pedestrian navigation with OpenStreetMap
support. By using the internal sensors of your smartphone, SmartNavi
saves up to 80% battery compared to other navigation apps. It detects your
steps and your direction to make you independent from GPS.
.

Repo Type:git
Repo:https://github.com/Phantast/smartnavi

Build:2.0.3,24
    commit=113afca0c02b0f4a327e1ba445dd3300d319319f
    subdir=smartnavi
    gradle=free
    prebuild=sed -i -e '/play-services/d' build.gradle
    disable=mapsforge.jar

Maintainer Notes:
https://github.com/Phantast/smartnavi/issues/2
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.0.3
Current Version Code:24


Categories:Phone & SMS
License:PublicDomain
Web Site:https://github.com/jbardera/RoamingInfo/blob/HEAD/README.md
Source Code:https://github.com/jbardera/RoamingInfo
Issue Tracker:https://github.com/jbardera/RoamingInfo/issues

Auto Name:Roaming Info
Summary:Notify about SIM operator changes
Description:
Displays a notification with current operator's name and in case of
roaming it also shows the original SIM's operator name.

If you swipe (dismiss) the notification, a new one will be displayed
when a change in the conexion occurs. To remove the service from
memory and avoid further notifications, just tap the notification.
.

Repo Type:git
Repo:https://github.com/jbardera/RoamingInfo

Build:1.7,12
    commit=e12465bc5ffa85565102220eace3b8267aed981e

Build:1.8,13
    commit=a381fdaead6a8f4c02e385e6ab69aff174f19b1c

Build:1.9,14
    commit=ee22ad40b1cc358d6ec77150b14956eb4f8b9d0e

Build:2.0,15
    commit=b1e5448c4e1e7a026208294946920f90cbcad0ab

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.1
Current Version Code:16


Categories:Office
License:Apache2
Web Site:https://github.com/gentlecat/Simple-Counter/blob/HEAD/README.md
Source Code:https://github.com/gentlecat/Simple-Counter/
Issue Tracker:https://github.com/gentlecat/Simple-Counter/issues
Changelog:https://github.com/gentlecat/Simple-Counter/blob/HEAD/CHANGELOG.md

Auto Name:Counter
Summary:Tally counter
Description:
Tally counter that makes counting easier. You can have multiple counters with
their own names and values. Values can be changed using volume buttons.
.

Repo Type:git
Repo:https://github.com/gentlecat/Simple-Counter/

Build:15,15
    commit=f6b3445105906c9bea1a70c452b780dc8ee3276c
    gradle=yes

Build:16,16
    commit=v16
    gradle=yes

Auto Update Mode:Version v%c
Update Check Mode:Tags
Current Version:16
Current Version Code:16

